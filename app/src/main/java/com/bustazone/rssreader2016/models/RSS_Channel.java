package com.bustazone.rssreader2016.models;

import com.bustazone.rssreader2016.models.RSS_Item;

import java.util.ArrayList;
import java.util.List;

public class RSS_Channel {

	private String title;
	private String link;
	private String description;
	private String image_url;
	private List<RSS_Item> items;
    
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public List<RSS_Item> getItems() {
		if (this.items==null)
		{
			this.items = new ArrayList<RSS_Item>();
		}
		return items;
	}
	public void setItems(List<RSS_Item> items) {
		this.items = items;
	}
	public void addItem(RSS_Item item) {
		if (this.items==null)
		{
			this.items = new ArrayList<RSS_Item>();
		}
		this.items.add(item);
	}
	public RSS_Item getItemById (int itemId) {
		for (RSS_Item it: items) {
			if (it.getId() == itemId)
			{
				return it;
			}
		}
		return null;
	}
	public List<RSS_Item> getItemsByTitle (String filter) {
		if (filter == null || filter.equals(""))
		{
			return items;
		}

		List<RSS_Item> out = new ArrayList<RSS_Item>();
		for (RSS_Item it: items) {
			if (it.getTitle().toLowerCase().contains(filter.toLowerCase()))
			{
				out.add(it);
			}
		}
		return out;
	}
}
