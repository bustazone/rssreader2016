package com.bustazone.rssreader2016;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.bustazone.rssreader2016.adapters.RSSItemsAdapter;
import com.bustazone.rssreader2016.dataUtils.serializedData;
import com.bustazone.rssreader2016.interfaces.AsyncRequestCallbacks;
import com.bustazone.rssreader2016.models.RSS_Channel;
import com.bustazone.rssreader2016.models.RSS_Item;

import java.util.List;


public class RSSItemListActivity extends AppCompatActivity implements AsyncRequestCallbacks {

    public boolean mTwoPane;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RSSItemsAdapter adapter;

    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;

    private RSS_Channel channel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rssitem_list);

        channel = serializedData.getInstance(this).getCurrentData(false, this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        recyclerView = (RecyclerView) findViewById(R.id.rssitem_list);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayoutList);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                serializedData.getInstance(RSSItemListActivity.this).getCurrentData(true, RSSItemListActivity.this);
            }
        });

        adapter = new RSSItemsAdapter(channel.getItems(), this);
        recyclerView.setAdapter(adapter);

        if (findViewById(R.id.rssitem_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_settings);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    List<RSS_Item> res = channel.getItemsByTitle(query);
                    if (res != null) {
                        adapter.setmValues(res);
                    }
                    adapter.notifyDataSetChanged();
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);


            SearchView.OnCloseListener ocl = new SearchView.OnCloseListener() {

                @Override
                public boolean onClose() {
                    adapter.setmValues(channel.getItems());
                    adapter.notifyDataSetChanged();
                    if (mSwipeRefreshLayout != null) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                    return false;
                }
            };

            searchView.setOnCloseListener(ocl);
        }

        return super.onCreateOptionsMenu(menu);
    }

    public static final int RESULT_SETTINGS = 1;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, UserSettingActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RESULT_SETTINGS:
                if (getSupportFragmentManager().findFragmentById(R.id.rssitem_detail_container) != null) {
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.rssitem_detail_container)).commit();
                }

                if (mSwipeRefreshLayout != null) {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
                serializedData.getInstance(this).getCurrentData(true, this);
                break;

        }

    }

    @Override
    public void onSuccess(RSS_Channel channel) {
        this.channel = channel;
        adapter.setmValues(this.channel.getItems());
        adapter.notifyDataSetChanged();
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onError(String errorMsg) {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        Toast.makeText(this,errorMsg,Toast.LENGTH_SHORT).show();
    }
}
