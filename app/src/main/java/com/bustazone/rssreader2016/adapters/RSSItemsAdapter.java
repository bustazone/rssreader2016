package com.bustazone.rssreader2016.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.volley.toolbox.NetworkImageView;
import com.bustazone.rssreader2016.R;
import com.bustazone.rssreader2016.RSSItemDetailActivity;
import com.bustazone.rssreader2016.RSSItemDetailFragment;
import com.bustazone.rssreader2016.RSSItemListActivity;
import com.bustazone.rssreader2016.RSSReaderApp;
import com.bustazone.rssreader2016.models.RSS_Item;

import java.util.List;

public class RSSItemsAdapter extends RecyclerView.Adapter<RSSItemsAdapter.ViewHolder> {

    private List<RSS_Item> mValues;
    private final RSSItemListActivity mCtx;

    public RSSItemsAdapter(List<RSS_Item> items, RSSItemListActivity mCtx) {
        this.mValues = items;
        this.mCtx = mCtx;
    }

    public void setmValues(List<RSS_Item> mValues) {
        this.mValues = mValues;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rssitem_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        RSS_Item it = mValues.get(position);

        holder.mItem = it;

        //Set list element title
        holder.tv_title.setText(it.getTitle());

        //Set list element desc
        holder.tv_desc.setText(Html.fromHtml(it.getDescription()));

        //Get item image
        String imageUrl = null;
        if (it.getImage_url()!=null) {
            imageUrl = it.getImage_url();
        }
        else if (it.getDescription()!=null)
        {
            if (it.getDescription().split("img").length>1)
            {
                if (it.getDescription().split("img")[1].split("src=\"").length>1)
                {
                    imageUrl = it.getDescription().split("img")[1].split("src=\"")[1].split("\"")[0];
                }
            }
        }

        //set list element image
        holder.iv_image.setDefaultImageResId(R.drawable.logo_01);
        holder.iv_image.setErrorImageResId(R.drawable.logo_01);
        if (imageUrl == null){
            holder.iv_image.setImageUrl(null, ((RSSReaderApp) mCtx.getApplication()).getImageLoader());
        }else if (!imageUrl.equals(holder.iv_image.getTag() == null ? "null" : holder.iv_image.getTag().toString())){
            if (imageUrl != null && imageUrl != "null"){
                holder.iv_image.setImageUrl(imageUrl, ((RSSReaderApp) mCtx.getApplication()).getImageLoader());
            }
        }else{
            holder.iv_image.setImageUrl(null, ((RSSReaderApp) mCtx.getApplication()).getImageLoader());
        }

        //set list element click listener
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCtx.mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(RSSItemDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId()));
                    RSSItemDetailFragment fragment = new RSSItemDetailFragment();
                    fragment.setArguments(arguments);
                    mCtx.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.rssitem_detail_container, fragment)
                            .commit();
                } else {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, RSSItemDetailActivity.class);
                    intent.putExtra(RSSItemDetailFragment.ARG_ITEM_ID, String.valueOf(holder.mItem.getId()));

                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final NetworkImageView iv_image;
        public final TextView tv_title;
        public final TextView tv_desc;
        public RSS_Item mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            iv_image = (NetworkImageView) view.findViewById(R.id.item_image);
            tv_title = (TextView) view.findViewById(R.id.textViewTitle);
            tv_desc = (TextView) view.findViewById(R.id.textViewDesc);

        }
    }
}