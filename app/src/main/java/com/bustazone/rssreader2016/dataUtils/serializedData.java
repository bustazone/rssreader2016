package com.bustazone.rssreader2016.dataUtils;

import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.bustazone.rssreader2016.R;
import com.bustazone.rssreader2016.SAXParser.RSS_Url_Load_Async;
import com.bustazone.rssreader2016.interfaces.AsyncRequestCallbacks;
import com.bustazone.rssreader2016.models.RSS_Channel;
import com.bustazone.rssreader2016.models.RSS_Item;
import com.google.gson.Gson;

public class serializedData implements AsyncRequestCallbacks {

	private AsyncRequestCallbacks mCallbacks;
	private Context mCtx;

	private RSS_Channel currentData;

	private static serializedData INSTANCE;

	public static serializedData getInstance(Context ctx) {
		if (INSTANCE == null)
		{
			INSTANCE = new serializedData(ctx);
		} else {
			INSTANCE.mCtx = ctx;
		}
		return INSTANCE;
	}

	public serializedData (Context ctx) {
		mCtx = ctx;
	}

	public RSS_Channel getCurrentData(Boolean forceRefresh, AsyncRequestCallbacks callbacks) {

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mCtx);
		String url = sharedPrefs.getString("prefUrlList", null);

		if (sharedPrefs.getBoolean("prefUrlPers", false)) {
			url = sharedPrefs.getString("prefUrl", null);
		}

		if (url==null || url.equals(""))
		{
			url = "http://www.xatakandroid.com/tag/feeds/rss2.xml";
			//url = "https://api.flickr.com/services/feeds/photos_public.gne?format=rss2";
		}

		currentData = getChannel(url, mCtx, callbacks, forceRefresh);

		return currentData;
	}
	
	public static Boolean isConnected (Context ctx)
	{
		 ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
	     NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	     return activeNetworkInfo != null; 
	}
	
	public RSS_Channel getChannel (String Url, Context ctx, AsyncRequestCallbacks callback, Boolean forcedUpdate)
	{
		mCallbacks = callback;
		mCtx = ctx;
			
		//Si no hay conexion se dispara error
		if (!isConnected(ctx))
		{
			if (mCallbacks != null)
				mCallbacks.onError(mCtx.getString(R.string.connection_problems));
		}
		
		//Si no hay conexion o no se ha forzado la descarga y adem�s existe un serializado anterior se devuelve dicho serializado.
		if ((!isConnected(ctx) || !forcedUpdate) && serializableUtils.existFile(ctx, ctx.getString(R.string.filename_serialized_channel)))
		{			
			String recs = (String) serializableUtils.deserialize(ctx, ctx.getString(R.string.filename_serialized_channel));

			if (recs.indexOf("{")>=0)
			{
				return (RSS_Channel) new Gson().fromJson(recs.substring(recs.indexOf("{")), new com.google.gson.reflect.TypeToken<RSS_Channel>(){}.getType());
			}
		}
		//En otro caso (se ha forzado la descarga o no existe un serializado anterior), si hay conexion se descarga
		else if (isConnected(ctx))
		{
			new RSS_Url_Load_Async(Url, this).execute();
		}	
	
		//Si se ha hecho la peticion o no se ha encontrado un serializado, se devuelve un channel vacio.
		return new RSS_Channel();
	}

	@Override
	public void onSuccess(RSS_Channel channel) {
		//Ordenar por fecha
		Collections.sort(channel.getItems(), new Comparator<RSS_Item>() {
			public int compare(RSS_Item o1, RSS_Item o2) {
		    	return o1.getPub_dateParsed().compareTo(o2.getPub_dateParsed());
			}
		});
		//Serializamos la devolucion con formato json
		String jso = new Gson().toJson(channel, new com.google.gson.reflect.TypeToken<RSS_Channel>(){}.getType());
		serializableUtils.serialize(mCtx, mCtx.getString(R.string.filename_serialized_channel), jso);
		currentData = channel;
		mCallbacks.onSuccess(channel);
	}

	@Override
	public void onError(String errorMsg) {
		Log.e("Request Problems", "mError_Msg ---> "+errorMsg);
		mCallbacks.onError(mCtx.getString(R.string.request_problems));
	}
}
