package com.bustazone.rssreader2016.interfaces;

import com.bustazone.rssreader2016.models.RSS_Channel;

public interface AsyncRequestCallbacks {
	public void onSuccess (RSS_Channel channel);
	public void onError (String errorMsg);
}
