package com.bustazone.rssreader2016.SAXParser;

import android.os.AsyncTask;

import com.bustazone.rssreader2016.SAXParser.RSS_SAX_Parser;
import com.bustazone.rssreader2016.interfaces.AsyncRequestCallbacks;
import com.bustazone.rssreader2016.models.RSS_Channel;


public class RSS_Url_Load_Async extends AsyncTask<Void, Void, RSS_Channel> {
	
	private Boolean mError;
	private String mError_Msg;
	
	private AsyncRequestCallbacks callbacks;
	private String url;
	
	public RSS_Url_Load_Async(String pUrl, AsyncRequestCallbacks pCallbacks) {
		this.callbacks = pCallbacks;
		this.url = pUrl;
	}
 
    protected RSS_Channel doInBackground(Void... params) {
 
    	try {
    		
            RSS_SAX_Parser saxparser = new RSS_SAX_Parser(this.url);
            
            RSS_Channel channel = saxparser.parse();

			mError = false;

            return channel;
            
		} catch (Exception e) {

			mError = true;
			mError_Msg = e.getMessage();

		}
    	
    	return null;
 
    }
 
    protected void onPostExecute(RSS_Channel result) {
 
    	if (!mError) {
    		this.callbacks.onSuccess(result);
    	} else {
			this.callbacks.onError(mError_Msg);
		}
    	
    }
}
